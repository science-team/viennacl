viennacl (1.7.1+dfsg1-6) unstable; urgency=medium

  * Team upload
  * Allow stderr output in autopkgtests for GCC warnings on armhf

 -- Graham Inggs <ginggs@debian.org>  Sun, 04 Oct 2020 12:52:41 +0000

viennacl (1.7.1+dfsg1-5) unstable; urgency=medium

  * [02bf939] Do not install README and changelog in -dev package. (Closes: #901041)
  * [394bb54] Remove changelog-file from -doc
  * [a549661] Update versions in Breaks/Replaces

 -- Anton Gladky <gladk@debian.org>  Mon, 11 Jun 2018 19:57:01 +0200

viennacl (1.7.1+dfsg1-4) unstable; urgency=medium

  * Team upload.
  * [113b0e5] Add Breaks/Replaces for the libviennacl-doc. (Closes: #901041)

 -- Anton Gladky <gladk@debian.org>  Sat, 09 Jun 2018 16:33:26 +0200

viennacl (1.7.1+dfsg1-3) unstable; urgency=medium

  * Team upload.
  * [6d46561] Apply cme fix dpkg (Standards-Version: 4.1.4)
  * [488529c] Set compat-level 11
  * [860b5ae] Remove myself from uploaders
  * [09dd8a8] Fix path to docs

 -- Anton Gladky <gladk@debian.org>  Thu, 07 Jun 2018 20:37:14 +0200

viennacl (1.7.1+dfsg1-2) unstable; urgency=medium

  * [1135942] Update d/gbp.conf.
  * [b6bf1f9] Move libboost-dev as a dependency of libviennacl-dev.
  * [3e37d8e] Apply cme fix dpkg.

 -- Anton Gladky <gladk@debian.org>  Sun, 04 Sep 2016 08:07:26 +0200

viennacl (1.7.1+dfsg1-1) unstable; urgency=medium

  * [7cbbc94] Update d/watch.
  * [fa23c51] Add Files-Excluded to d/copyright.
  * [4ff9269] Use packaged jquery.js
  * [146f730] Remove unused d/patch.
  * [49e9931] New upstream version 1.7.1+dfsg1
  * [678e017] Use simpler doc-file.
  * [932026d] Apply cme fix dpkg.
  * [f9402fb] Fix duplicated licenses in d/copyright.

 -- Anton Gladky <gladk@debian.org>  Mon, 25 Jan 2016 10:53:04 +0100

viennacl (1.5.2-2) unstable; urgency=medium

  [ Graham Inggs ]
  * [6fb521e] Add ocl-icd-opencl-dev | opencl-dev to BD.
              (Closes: #760232, #761528)

  [ Anton Gladky ]
  * [bee9ecc] Fix license name.
  * [8934d52] Set canonical VCS-field.
  * [029cb4e] Use wrap-and-sort.
  * [9fecf0c] Add autopkgtest.

 -- Anton Gladky <gladk@debian.org>  Thu, 18 Sep 2014 23:07:51 +0200

viennacl (1.5.2-1) unstable; urgency=medium

  * [15d0b5a] New upstream version 1.5.2
    + bug fixes and performance improvements

 -- Toby Smithe <tsmithe@ubuntu.com>  Fri, 16 May 2014 11:43:59 +0100

viennacl (1.5.1-1) unstable; urgency=medium

  [ Toby Smithe ]

  * [d19020e] New upstream version 1.5.1
  * Update debian/control:
    + add myself to Uploaders;
    + update Standards-Version to 3.9.5;
    + move out of contrib.

  [ Michael Wild ]

  * [c10d1b6] Removed patches/*, everything fixed in new upstream
  * [ac8d81b] Exclude editor backup file from dh_clean
  * [8cf95b2] Don't auto-generate changelog anymore, upstream added it
  * [1e6e73c] Remove get-orig-source target; upstream is clean
  * [7975906] Remove erronous DMUA entry
  * [a5a30b6] New upstream version 1.3.0
  * [2db3b5a] Update DH_ALWAYS_EXCLUDE for 1.3.0
  * [0f8c6fd] Configure debian/libviennacl-doc.docs in
              override_dh_auto_configure
  * [b328905] Remove Ubuntu-specific include-directory
  * [359dede] Remove unused build-deps: poppler-utils, asciidoc, lynx
  * [a43ff39] Add 0001-Add-virtual-dtor-to-abstract-result_of-class.patch
  * [a86c820] Add 0002-Fix-declaration-order-of-prod_impl-trans_prod_impl.patch
  * [3456f0e] Add 0003-Generate-kernel-sources-in-build-tree.patch
  * [c70a562] Install headers from DESTDIR instead from source
  * [90ec80a] Cleanup and comments in debian/rules
  * [a99add4] Disable python-support in debian/rules
  * [35a31dc] Configure git-dch to include hashes in the entries
  * [afa8f8d] Standardize Vcs-Git URL
  * [914f232] Upload to experimental because of Wheezy freeze

  [ Anton Gladky ]

  * [35433e9] Set compat level 9.
  * [33bf385] Enable parallel build.

 -- Toby Smithe <tsmithe@ubuntu.com>  Wed, 19 Feb 2014 16:12:04 +0000

viennacl (1.2.0-2) unstable; urgency=low

  * [432b5df] Change debian branch to debian/1.2.0
  * [815d6d8] Fix declaration order of prod_impl() and trans_prod_impl()
    - Added d/p/0004-Fix-declaration-order-of-prod_impl-trans_prod_impl.patch
    (Closes: 682410)

 -- Michael Wild <themiwi@users.sourceforge.net>  Fri, 27 Jul 2012 22:24:56 +0200

viennacl (1.2.0-1) unstable; urgency=low

  * Make debian/copyright lintian clean
  * Improve debian/gbp.conf
  * New upstream version 1.2.0
  * Reworked packaging for new build system
  * Removed obsolete patches
  * Added 0001-Define-OPENCL_INCLUDE_DIRS-in-cmake-FindOpenCL.cmake.patch
  * Updated debian/watch file for appended -src suffix
  * Bump required cmake version to 2.8
  * Added get-orig-source target to debian/rules
  * Added 0002-Double-precision-on-AMD-GPUs-in-version-1.2.0.patch
  * Added 0003-Fix-adding-a-new-compute-device.patch
  * Update Standards-Version to 3.9.3

 -- Michael Wild <themiwi@users.sourceforge.net>  Mon, 27 Feb 2012 10:00:46 +0100

viennacl (1.1.2-6) unstable; urgency=low

  * Copyright info for CL/* external/pugixml/*

 -- Michael Wild <themiwi@users.sourceforge.net>  Tue, 02 Aug 2011 10:30:34 +0200

viennacl (1.1.2-5) unstable; urgency=low

  * Fix regex in debian/watch
  * Remove generated files and re-generate when building
  * Link auxiliary/converter against libboost_system-mt
  * Force the Boost.Filesystem version 2 API for auxiliary/converter.cpp

 -- Michael Wild <themiwi@users.sourceforge.net>  Tue, 02 Aug 2011 09:35:51 +0200

viennacl (1.1.2-4) unstable; urgency=low

  * Bump Standards-Version to 3.9.2

 -- Michael Wild <themiwi@users.sourceforge.net>  Tue, 12 Jul 2011 19:45:08 +0200

viennacl (1.1.2-3) unstable; urgency=low

  * Use dpkg-mergechangelogs to merge debian/changelog
  * Build tests verbosely

 -- Michael Wild <themiwi@users.sourceforge.net>  Tue, 05 Jul 2011 19:39:40 +0200

viennacl (1.1.2-2) unstable; urgency=low

  * Add patch to properly link against libOpenCL

 -- Michael Wild <themiwi@users.sourceforge.net>  Tue, 05 Jul 2011 17:26:15 +0200

viennacl (1.1.2-1) unstable; urgency=low

  * Initial release (Closes: #620135)

 -- Michael Wild <themiwi@users.sourceforge.net>  Mon, 04 Jul 2011 16:14:19 +0200
