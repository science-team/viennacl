#include <stdio.h>
#include <CL/cl.h>

int main()
{
  cl_platform_id pform_id;
  if (clGetPlatformIDs(1, &pform_id, NULL) == CL_SUCCESS)
  {
    cl_uint ndevices;
    if (clGetDeviceIDs(pform_id, CL_DEVICE_TYPE_ALL, 0, NULL, &ndevices) == CL_SUCCESS)
    {
      return 0;
    }
  }
  return 1;
}
